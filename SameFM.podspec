Pod::Spec.new do |s|

  s.name         = "SameFM"
  s.version      = "0.0.1"
  s.summary      = "SameFM"

  s.description  = <<-DESC
                   A longer description of StreamPlayer in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC

  s.homepage     = "http://git.oschina.net/pomin/VoiceDemo"
  s.license      = 'MIT'
  s.author       = { "Pomin" => "9834784@qq.com" }
  s.platform     = :ios, '5.0'
  s.source       = { :git => "https://git.oschina.net/pomin/VoiceDemo.git", :tag => "0.0.1" }

  s.source_files  = 'SameFM/SameFM/*.{h,m}'
  s.frameworks    = 'MediaPlayer', 'AVFoundation', 'XCTest'
  s.requires_arc  = true
  s.dependency "KVOController"
  s.dependency "StreamPlayer"
  
  # s.dependency "MagicalRecord", :git => "https://github.com/iiiyu/MagicalRecord.git", :tag => "sumiGridDiary2.1"

end

//
//  CardFlowView.h
//  FlowView
//
//  Created by yixia on 13-8-20.
//  Copyright (c) 2013年 heyun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class CardView;
@protocol CardFlowViewDataSource;

@interface CardFlowView : UIView

@property (nonatomic, PM_WEAK) id<CardFlowViewDataSource> delegate;

// size of cover view
@property (nonatomic, assign) CGSize cardSize;
// space between cover views
@property (nonatomic, assign) CGFloat cardSpace;
// angle of side cover views
@property (nonatomic, assign) CGFloat cardAngle;
// scale of middle cover view
@property (nonatomic, assign) CGFloat cardScale;

@property (nonatomic, assign, readonly) NSInteger numberOfCards;

- (void)reloadData;
- (void)scrollToIndex:(NSInteger)index animated:(BOOL)animated;
- (void)insertDataWithIndex:(NSInteger)index animated:(BOOL)animated;

- (CardView *)leftVisibleCard;
- (CardView *)rightVisibleCard;

@end

#pragma mark - Card Flow Data Source
@protocol CardFlowViewDataSource <NSObject>

- (NSInteger)numberOfCards:(CardFlowView *)coverFlowView;
- (CardView *)cardFlowView:(CardFlowView *)coverFlowView cardAtIndex:(NSInteger)index;

@end


#pragma mark - CardView
@interface CardView : UIView

@property (nonatomic, assign) NSInteger index;

// the card image view
@property (nonatomic, PM_STRONG, readonly) UIImageView *imageView;

// the card background view
@property (nonatomic, PM_STRONG) UIView *backgroundView;

@end
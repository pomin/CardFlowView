//
//  AppDelegate.h
//  FlowView
//
//  Created by yixia on 13-8-20.
//  Copyright (c) 2013年 heyun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end

//
//  ViewController.m
//  SLcardFlow
//
//  Created by SmartCat on 13-6-13.
//  Copyright (c) 2013年 SmartCat. All rights reserved.
//

#import "ViewController.h"
#import "CardFlowView.h"
#import "CardBackgroundView.h"


static const CGFloat SLcardViewWidth = 208.0;
static const CGFloat SLcardViewHeight = 327.0;
static const CGFloat SLcardViewSpace = 100.0;
static const CGFloat SLcardViewAngle = M_PI_4;
static const CGFloat SLcardViewScale = 1.0;

@interface ViewController () <CardFlowViewDataSource> {
    CardFlowView *_cardFlowView;
    NSMutableArray *_colors;
    
    UISlider *_widthSlider;
    UISlider *_heightSlider;
    UISlider *_spaceSlider;
    UISlider *_angleSlider;
    UISlider *_scaleSlider;
}

@end

@implementation ViewController

- (void)loadView {
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGRect frame = self.view.bounds;
    frame.size.height = 442;
    _cardFlowView = [[CardFlowView alloc] initWithFrame:frame];
    _cardFlowView.backgroundColor = [UIColor grayColor];
    _cardFlowView.delegate = self;
    _cardFlowView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    _cardFlowView.cardSize = CGSizeMake(SLcardViewWidth, SLcardViewHeight);
    _cardFlowView.cardSpace = 31.0;
    _cardFlowView.cardAngle = 0.0;
    _cardFlowView.cardScale = 1.1;
    [self.view addSubview:_cardFlowView];
    
    
    UIButton *b = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    b.frame = CGRectMake(20, CGRectGetMaxY(_cardFlowView.frame) + 20.0, 100, 60);
    [b setBackgroundColor:[UIColor purpleColor]];
    [b addTarget:self action:@selector(btnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b];
    
    // width
//    _widthSlider = [self addSliderWithMinY:(CGRectGetMaxY(_cardFlowView.frame) + 20.0) labelText:@"Width:"];
//    _cardFlowView.cardSize = CGSizeMake(SLcardViewWidth * _widthSlider.value,
//                                          _cardFlowView.cardSize.height);
//    
//    // height
//    _heightSlider = [self addSliderWithMinY:(CGRectGetMaxY(_widthSlider.frame) + 20.0) labelText:@"Height:"];
//    [self.view addSubview:_heightSlider];
//    _cardFlowView.cardSize = CGSizeMake(_cardFlowView.cardSize.width,
//                                          SLcardViewHeight * _heightSlider.value);
//    
//    // space
//    _spaceSlider = [self addSliderWithMinY:(CGRectGetMaxY(_heightSlider.frame) + 20.0) labelText:@"Space:"];
//    _cardFlowView.cardSpace = _spaceSlider.value * SLcardViewSpace;
//    
//    // angle
//    _angleSlider = [self addSliderWithMinY:(CGRectGetMaxY(_spaceSlider.frame) + 20.0) labelText:@"Angle:"];
//    _cardFlowView.cardAngle = _angleSlider.value * SLcardViewAngle;
//    
//    // scale
//    _scaleSlider = [self addSliderWithMinY:(CGRectGetMaxY(_angleSlider.frame) + 20.0) labelText:@"Scale:"];
//    _cardFlowView.cardScale = _scaleSlider.value * SLcardViewScale;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _colors = [[NSMutableArray arrayWithCapacity:50] retain];
    for (NSInteger i = 0; i < 5; ++i) {
        float red = rand() % 255;
        float green = rand() % 255;
        float blue = rand() % 255;
        UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1.0];
        [_colors addObject:color];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_cardFlowView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)valueChanged:(id)sender {
    UISlider *slider = (UISlider *)sender;
    if ([slider isEqual:_widthSlider]) {
        _cardFlowView.cardSize = CGSizeMake(SLcardViewWidth * _widthSlider.value,
                                              _cardFlowView.cardSize.height);
    } else if ([slider isEqual:_heightSlider]) {
        _cardFlowView.cardSize = CGSizeMake(_cardFlowView.cardSize.width,
                                              SLcardViewHeight * _heightSlider.value);
    } else if ([slider isEqual:_spaceSlider]) {
        _cardFlowView.cardSpace = _spaceSlider.value * SLcardViewSpace;
    } else if ([slider isEqual:_angleSlider]) {
        _cardFlowView.cardAngle = _angleSlider.value * SLcardViewAngle;
    } else if ([slider isEqual:_scaleSlider]) {
        _cardFlowView.cardScale = _scaleSlider.value * SLcardViewScale;
    }
}

#pragma mark - SLcardFlowViewDataSource

- (NSInteger)numberOfCards:(CardFlowView *)cardFlowView
{
    return _colors.count;
}

- (CardView *)cardFlowView:(CardFlowView *)cardFlowView cardAtIndex:(NSInteger)index
{
    CardView *view = [[CardView alloc] initWithFrame:CGRectMake(0.0, 0.0, SLcardViewWidth, SLcardViewHeight)];
    view.index = index;
    
    view.imageView.image = [UIImage imageNamed:@"SoundCard_BG"];
    CGRect labelFrame = CGRectMake(20.0, 0, 80.0, 30.0);
    UILabel *label = [[UILabel alloc] initWithFrame:labelFrame];
//    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:22.0];
    label.text = [NSString stringWithFormat:@"index: %d", index];
    label.textColor = [UIColor darkTextColor];
    [view addSubview:label];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

#pragma mark - Private methods

- (UISlider *)addSliderWithMinY:(CGFloat)minY labelText:(NSString *)labelText {
    CGRect labelFrame = CGRectMake(20.0, minY, 80.0, 30.0);
    UILabel *label = [[UILabel alloc] initWithFrame:labelFrame];
    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:22.0];
    label.text = labelText;
    label.textColor = [UIColor darkTextColor];
    [self.view addSubview:label];
    
    CGRect sliderFrame = CGRectMake(CGRectGetMaxX(labelFrame) + 20.0, minY, 200.0, 30.0);
    sliderFrame.size.width = CGRectGetWidth(self.view.bounds) - CGRectGetMaxX(labelFrame) - 40.0;
    UISlider *slider = [[UISlider alloc] initWithFrame:sliderFrame];
    slider.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    slider.minimumValue = 0.0;
    slider.maximumValue = 2.0;
    slider.value = 1.0;
    [slider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:slider];
    return slider;
}

- (void)btnTap:(id)sender
{
    NSLog(@"tap");
    float red = rand() % 255;
    float green = rand() % 255;
    float blue = rand() % 255;
    UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1.0];
    NSInteger index = _colors.count;
    [_colors insertObject:color atIndex:index];
    [_cardFlowView insertDataWithIndex:index animated:YES];
}

@end

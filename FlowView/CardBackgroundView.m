//
//  CardBackgroundView.m
//  FlowView
//
//  Created by yixia on 13-8-21.
//  Copyright (c) 2013年 heyun. All rights reserved.
//

#import "CardBackgroundView.h"

@interface CardBackgroundView ()
{
    CGRect _coloredBoxRect;
    CGRect _paperRect;
}

@end

@implementation CardBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _coloredBoxRect = CGRectMake(0, 0, frame.size.width, 208);
        _paperRect = CGRectMake(0, 208, frame.size.width, frame.size.height - 208);
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGColorRef grayColor = [UIColor colorWithRed:115.0/255.0 green:115.0/255.0 blue:115.0/255.0 alpha:1.0].CGColor;
    CGColorRef whiteColor = [UIColor whiteColor].CGColor;
    
    CGContextSetFillColorWithColor(context, grayColor);
    CGContextFillRect(context, _coloredBoxRect);
    
    CGContextSetFillColorWithColor(context, whiteColor);
    CGContextFillRect(context, _paperRect);
}


@end

//  ARC Helper ends

#ifndef PMlayer_ARCHelper

#if __has_feature(objc_arc_weak)                //objc_arc_weak
#define PM_WEAK weak
#define __PM_WEAK __weak
#define PM_STRONG strong

#define PM_AUTORELEASE self
#define PM_RELEASE self
#define PM_RETAIN self
#define PM_CFTYPECAST(exp) (__bridge exp)
#define PM_TYPECAST(exp) (__bridge_transfer exp)
#define PM_CFRELEASE(exp) CFRelease(exp)
#define PM_DEALLOC self

#elif __has_feature(objc_arc)                   //objc_arc
#define PM_WEAK unsafe_unretained
#define __PM_WEAK __unsafe_unretained
#define PM_STRONG strong

#define PM_AUTORELEASE self
#define PM_RELEASE self
#define PM_RETAIN self
#define PM_CFTYPECAST(exp) (__bridge exp)
#define PM_TYPECAST(exp) (__bridge_transfer exp)
#define PM_CFRELEASE(exp) CFRelease(exp)
#define PM_DEALLOC self

#else                                           //none
#define PM_WEAK assign
#define __PM_WEAK
#define PM_STRONG retain

#define PM_AUTORELEASE autorelease
#define PM_RELEASE release
#define PM_RETAIN retain
#define PM_CFTYPECAST(exp) (exp)
#define PM_TYPECAST(exp) (exp)
#define PM_CFRELEASE(exp) CFRelease(exp)
#define PM_DEALLOC dealloc
#define __bridge

#endif                                          //__has_feature for ARC



#if __has_feature(objc_instancetype)            //objc_instancetype
#define PM_INSTANCETYPE instancetype
#else
#define PM_INSTANCETYPE id
#endif                                          //__has_feature for instancetype

#define PMLog(format, ...) NSLog(@"%s:%@";, __PRETTY_FUNCTION__,[NSString stringWithFormat:format, ## __VA_ARGS__]);

#endif

#define END_TIMER(msg)  NSTimeInterval stop = [NSDate timeIntervalSinceReferenceDate]; 


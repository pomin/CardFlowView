//
//  CardFlowView.m
//  FlowView
//
//  Created by yixia on 13-8-20.
//  Copyright (c) 2013年 heyun. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "CardFlowView.h"

#pragma mark - 卡片容器
#pragma mark CardWrapper
@interface CardWrapper : NSObject
/**
 *	卡片容器
 */
@property (nonatomic, PM_WEAK) CardView *cardView;
@property (nonatomic, assign) NSInteger index;

@end

@implementation CardWrapper
@end



#pragma mark - 卡片流滚动容器
#pragma mark CardFlowScrollView
@interface CardFlowScrollView : UIScrollView
{
    UIView *_container;
    
    CGFloat _horzMargin;
    CGFloat _vertMargin;
    
    NSMutableArray *_cardWrappers;
}

@property (nonatomic, PM_WEAK) CardFlowView *parentView;

- (CardView *)leftVisibleCard;
- (CardView *)rightVisibleCard;

- (void)reloadDate;
- (void)removeAllCards;
- (void)repositionVisibleCards;

@end

@implementation CardFlowScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.parentView.numberOfCards > 0)
    {
        // tile cover view in visible bounds
        CGRect visibleBounds = [self convertRect:[self bounds] toView:_container];
        [self tileCardsFromMinX:CGRectGetMinX(visibleBounds) toMaxX:CGRectGetMaxX(visibleBounds)];
        
        // adjust the (3D)attributes of the visible cover views
        [self adjustCardsTransformWithVisibleBounds:visibleBounds];
    }
}

- (void)dealloc
{
    [_container PM_RELEASE];
    
    [self removeAllCards];
    [_cardWrappers PM_RELEASE];
    
#if !__has_feature(objc_arc)                   //objc_arc
    [super dealloc];
#endif
}

#pragma mark Instance methods
- (CardView *)leftVisibleCard
{
    if (_cardWrappers.count) {
        return [[_cardWrappers objectAtIndex:0] cardView];
    }
    return nil;
}

- (CardView *)rightVisibleCard
{
    if (_cardWrappers.count) {
        return [[_cardWrappers lastObject] cardView];
    }
    return nil;
}

- (void)reloadDate
{
    [self removeAllCards];
    
    CGPoint _offset = self.contentOffset;
    [self resetContentSize];
    
    if (self.contentSize.width < _offset.x + self.bounds.size.width) {
        _offset.x = self.contentSize.width - self.bounds.size.width;
    }
    
    _offset.x = _offset.x < 0 ? 0 : _offset.x;
    _offset.y = _offset.y < 0 ? 0 : _offset.y;
    
    self.contentOffset = _offset;
}

- (void)removeAllCards
{
    for (CardWrapper *wrapper in _cardWrappers) {
        [wrapper.cardView removeFromSuperview];
    }
    [_cardWrappers removeAllObjects];
}

- (void)repositionVisibleCards
{
    // reset the top, left, right, bottom margin
    CGFloat _vHorzMargin = _horzMargin;
    CGFloat _vVertMargin = _vertMargin;
    [self resetContentSize];
    
    // adjust the position
    for (int i = 0; i < _cardWrappers.count; ++i) {
        CardView *view = [[_cardWrappers objectAtIndex:i] cardView];
        view.center = CGPointMake(view.center.x + _horzMargin - _vHorzMargin,
                                  view.center.y + _vertMargin - _vVertMargin);
    }
}

#pragma mark Private methods
- (void)setup
{
    _container = [[UIView alloc] initWithFrame:self.bounds];
    _container.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    _container.backgroundColor = [UIColor clearColor];
    [self addSubview:_container];
    
    _horzMargin = 0.0;
    _vertMargin = 0.0;
    _cardWrappers = [[NSMutableArray alloc] init];
}

- (void)resetContentSize
{
    // reset the top, left, right, bottom margin
    _horzMargin = (CGRectGetWidth(self.frame) - self.parentView.cardSize.width)/2.0;
    _vertMargin = (CGRectGetHeight(self.frame) - self.parentView.cardSize.height)/2.0;
    
    // reset content size
    if (self.parentView.numberOfCards > 0) {
        self.contentSize = CGSizeMake(_horzMargin*2.0 + self.parentView.numberOfCards*self.parentView.cardSize.width + (self.parentView.numberOfCards-1)*self.parentView.cardSpace, self.frame.size.height);
    } else {
        self.contentSize = self.frame.size;
    }
    
    // reset frame of _container
    _container.frame = CGRectMake(0.0, 0.0, self.contentSize.width, self.contentSize.height);
}

- (CardView *)insertCardAtIndex:(NSInteger)index frame:(CGRect)frame
{
    CardView *card = [self.parentView.delegate cardFlowView:self.parentView cardAtIndex:index];
    card.frame = frame;
    [_container addSubview:card];
    return card;
}

- (CardWrapper *)addNewCardOnRight:(CGFloat)rightEdge index:(NSInteger)index
{
    CardWrapper *wrapper = nil;
    if (index >= 0 && index < self.parentView.numberOfCards)
    {
        CGRect frame = CGRectMake(rightEdge, _vertMargin, 0.0, 0.0);
        frame.size = self.parentView.cardSize;
        CardView *card = [self insertCardAtIndex:index frame:frame];
        
        wrapper = [[CardWrapper alloc] init];
        wrapper.index = index;
        wrapper.cardView = card;
        [_cardWrappers addObject:wrapper];
    }
    return wrapper;
}

- (CardWrapper *)addNewCardOnLeft:(CGFloat)leftEdge index:(NSInteger)index
{
    CardWrapper *wrapper = nil;
    if (index >= 0 && index < self.parentView.numberOfCards)
    {
        CGRect frame = CGRectMake(leftEdge - self.parentView.cardSize.width, _vertMargin, 0.0, 0.0);
        frame.size = self.parentView.cardSize;
        CardView *card = [self insertCardAtIndex:index frame:frame];
        
        wrapper = [[CardWrapper alloc] init];
        wrapper.index = index;
        wrapper.cardView = card;
        [_cardWrappers insertObject:wrapper atIndex:0];
    }
    return wrapper;
}

- (CGFloat)rightEdgeOfCardAtIndex:(NSInteger)index
{
    // right edge of the card view at index, space between card views must be considered
    return (_horzMargin + (self.parentView.cardSize.width + self.parentView.cardSpace)*(index + 1));
}

- (CGFloat)leftEdgeOfCardAtIndex:(NSInteger)index
{
    // left edge of the card view at index, space between card views must be considered
    return (_horzMargin + (self.parentView.cardSize.width + self.parentView.cardSpace)*index - self.parentView.cardSpace);
}

- (void)tileCardsFromMinX:(CGFloat)minimumVisibleX toMaxX:(CGFloat)maximumVisibleX
{
    // add the first card view
    if (0 == _cardWrappers.count)
    {
        // calculate the nearby middle card view index in the visible bounds
        NSInteger index = ceilf((minimumVisibleX - _horzMargin)/(self.parentView.cardSize.width + self.parentView.cardSpace));
        index = MIN(MAX(0, index), self.parentView.numberOfCards-1);
        // add card view at middle
        CGFloat rightEdge = _horzMargin + (self.parentView.cardSize.width + self.parentView.cardSpace)*index;
        [self addNewCardOnRight:rightEdge index:index];
    }
    
    // add card views missing at right
    CardWrapper *lastCardWrapper = (CardWrapper *)[_cardWrappers lastObject];
    CGFloat rightEdge = [self rightEdgeOfCardAtIndex:lastCardWrapper.index];
    while (rightEdge < maximumVisibleX) {
        lastCardWrapper = [self addNewCardOnRight:rightEdge index:(lastCardWrapper.index+1)];
        if (lastCardWrapper) {
            rightEdge = [self rightEdgeOfCardAtIndex:lastCardWrapper.index];
        } else {
            break;
        }
    }
    
    // add card views missing at left
    CardWrapper *firstCardWrapper = (CardWrapper *)[_cardWrappers objectAtIndex:0];
    CGFloat leftEdge = [self leftEdgeOfCardAtIndex:firstCardWrapper.index];
    while (leftEdge > minimumVisibleX) {
        firstCardWrapper = [self addNewCardOnLeft:leftEdge index:(firstCardWrapper.index - 1)];
        if (firstCardWrapper) {
            leftEdge = [self leftEdgeOfCardAtIndex:firstCardWrapper.index];
        } else {
            break;
        }
    }
    
    // remove card views out of left bounds
    firstCardWrapper = (CardWrapper *)[_cardWrappers objectAtIndex:0];
    while (firstCardWrapper &&
           CGRectGetMaxX(firstCardWrapper.cardView.frame) < minimumVisibleX) {
        [firstCardWrapper.cardView removeFromSuperview];
        [_cardWrappers removeObjectAtIndex:0];
        firstCardWrapper = [_cardWrappers objectAtIndex:0];
    }
    
    // remove card views out of right bounds
    lastCardWrapper = (CardWrapper *)[_cardWrappers lastObject];
    while (lastCardWrapper &&
           CGRectGetMinX(lastCardWrapper.cardView.frame) > maximumVisibleX) {
        [lastCardWrapper.cardView removeFromSuperview];
        [_cardWrappers removeLastObject];
        lastCardWrapper = [_cardWrappers lastObject];
    }
}

- (void)adjustCardsTransformWithVisibleBounds:(CGRect)visibleBounds
{
    // adjust scale and transform of all the visible views
    CGFloat visibleBoundsCenterX = CGRectGetMidX(visibleBounds);
    for (NSInteger i = 0; i < _cardWrappers.count; ++i)
    {
        UIView *card = [[_cardWrappers objectAtIndex:i] cardView];
        
        CGFloat distance = card.center.x - visibleBoundsCenterX;
        CGFloat distanceThreshold = self.parentView.cardSize.width + self.parentView.cardSpace;
        if (distance <= -distanceThreshold) {
            card.layer.transform = [self transform3DWithRotation:self.parentView.cardAngle
                                                           scale:1.0
                                                     perspective:(-1.0/500.0)];
            card.layer.zPosition = -10000.0;
        }
        else if (distance < 0.0 && distance > -distanceThreshold) {
            CGFloat percentage = fabsf(distance)/distanceThreshold;
            CGFloat scale = 1.0 + (self.parentView.cardScale - 1.0) * (1.0 - percentage);
            card.layer.transform = [self transform3DWithRotation:self.parentView.cardAngle * percentage
                                                           scale:scale
                                                     perspective:(-1.0/500.0)];
            card.layer.zPosition = -10000.0;
        }
        else if (distance == 0.0) {
            card.layer.transform = [self transform3DWithRotation:0.0
                                                           scale:self.parentView.cardScale
                                                     perspective:(1.0/500.0)];
            card.layer.zPosition = 10000.0;
        }
        else if (distance > 0.0 && distance < distanceThreshold) {
            CGFloat percentage = fabsf(distance)/distanceThreshold;
            CGFloat scale = 1.0 + (self.parentView.cardScale - 1.0) * (1.0 - percentage);
            card.layer.transform = [self transform3DWithRotation:-self.parentView.cardAngle * percentage
                                                           scale:scale
                                                     perspective:(-1.0/500.0)];
            card.layer.zPosition = -10000.0;
        }
        else if (distance >= distanceThreshold) {
            card.layer.transform = [self transform3DWithRotation:-self.parentView.cardAngle
                                                           scale:1.0
                                                     perspective:(-1.0/500.0)];
            card.layer.zPosition = -10000.0;
        }
    }
}

- (CATransform3D)transform3DWithRotation:(CGFloat)angle
                                   scale:(CGFloat)scale
                             perspective:(CGFloat)perspective {
    CATransform3D rotateTransform = CATransform3DIdentity;
    rotateTransform.m34 = perspective;
    rotateTransform = CATransform3DRotate(rotateTransform, angle, 0.0, 1.0, 0.0);
    
    CATransform3D scaleTransform = CATransform3DIdentity;
    scaleTransform = CATransform3DScale(scaleTransform, scale, scale, 1.0);
    
    return CATransform3DConcat(rotateTransform, scaleTransform);
}

@end



#pragma mark - Card Flow View
#pragma mark CardFlowView

static const CGFloat CardWidth = 100.0;
static const CGFloat CardHeight = 100.0;

@interface CardFlowView () <UIScrollViewDelegate>
{
    CardFlowScrollView *_scrollView;
    CGPoint _endDarggingVelocity;
}
@end

@implementation CardFlowView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)dealloc
{
    [_scrollView PM_RELEASE];
    
#if !__has_feature(objc_arc)                   //objc_arc
    [super dealloc];
#endif
}

#pragma mark Instance methods
- (void)reloadData
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(numberOfCards:)])
    {
        _numberOfCards = [self.delegate numberOfCards:self];
        [_scrollView reloadDate];
    }
}

- (void)scrollToIndex:(NSInteger)index animated:(BOOL)animated
{
    if (index < 0 || index >= _numberOfCards) return;
    
    [_scrollView setContentOffset:[self nearByOffsetOfScrollViewContentOffset:[self offsetWithCenterCoverViewIndex:index]] animated:animated];
}

- (void)insertDataWithIndex:(NSInteger)index animated:(BOOL)animated
{
    [self reloadData];
    
    [self scrollToIndex:index animated:animated];
}

- (void)insertAnimate:(NSDictionary *)userInfo
{
    NSInteger index = [userInfo[@"index"] integerValue];
    BOOL animated = [userInfo[@"animated"] boolValue];
    [self scrollToIndex:index animated:animated];
}

- (CardView *)leftVisibleCard
{
    return [_scrollView leftVisibleCard];
}

- (CardView *)rightVisibleCard
{
    return [_scrollView rightVisibleCard];
}

#pragma mark Setter methods
- (void)setFrame:(CGRect)frame
{
    if (!CGRectEqualToRect(self.frame, frame))
    {
        [super setFrame:frame];
        [_scrollView repositionVisibleCards];
    }
}

- (void)setCardSize:(CGSize)cardSize
{
    if (!CGSizeEqualToSize(_cardSize, cardSize))
    {
        NSInteger centerIndex = [self nearByIndexOfScrollViewContentOffset:_scrollView.contentOffset];
        _cardSize = cardSize;
        
        [_scrollView removeAllCards];
        [_scrollView resetContentSize];
        _scrollView.contentOffset = [self offsetWithCenterCoverViewIndex:centerIndex];
        [_scrollView setNeedsLayout];
    }
}

- (void)setCardSpace:(CGFloat)cardSpace
{
    NSInteger centerIndex = [self nearByIndexOfScrollViewContentOffset:_scrollView.contentOffset];
    _cardSpace = cardSpace;
    
    [_scrollView removeAllCards];
    [_scrollView resetContentSize];
    _scrollView.contentOffset = [self offsetWithCenterCoverViewIndex:centerIndex];
    [_scrollView setNeedsLayout];
}

- (void)setCardAngle:(CGFloat)cardAngle
{
    if (_cardAngle != cardAngle)
    {
        _cardAngle = cardAngle;
        
        [_scrollView setNeedsLayout];
    }
}

- (void)setCardScale:(CGFloat)cardScale
{
    if (_cardScale != cardScale)
    {
        _cardScale = cardScale;
        
        [_scrollView setNeedsLayout];
    }
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
//    scrollView.contentOffset = [self nearByOffsetOfScrollViewContentOffset:scrollView.contentOffset];
//    [scrollView setContentOffset:[self nearByOffsetOfScrollViewContentOffset:scrollView.contentOffset] animated:YES];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset
{
    _endDarggingVelocity = velocity;
    
    if (_endDarggingVelocity.x == 0) {
        // find the nearby content offset
        *targetContentOffset = [self nearByOffsetOfScrollViewContentOffset:_scrollView.contentOffset];
    }
    else {
        // calculate the slide distance and end scrollview content offset
        CGFloat startVelocityX = fabsf(_endDarggingVelocity.x);
        CGFloat decelerationRate = 1.0 - _scrollView.decelerationRate;
        
        CGFloat decelerationSeconds = startVelocityX / decelerationRate;
        CGFloat distance = startVelocityX * decelerationSeconds - 0.5 * decelerationRate * decelerationSeconds * decelerationSeconds;
        
        CGFloat endOffsetX = _endDarggingVelocity.x > 0 ? (_scrollView.contentOffset.x + distance) : (_scrollView.contentOffset.x - distance);
        
        // calculate the nearby content offset of the middle card view
        CGPoint nearByOffset = [self nearByOffsetOfScrollViewContentOffset:CGPointMake(endOffsetX, _scrollView.contentOffset.y)];
        
        // avoid boucing back
        int index = [self nearByIndexOfScrollViewContentOffset:nearByOffset];
        if (_endDarggingVelocity.x > 0) {
            if (nearByOffset.x < endOffsetX) {
                if (index < (_numberOfCards-1)) {
                    nearByOffset = [self offsetWithCenterCoverViewIndex:(index+1)];
                }
            }
        } else {
            if (nearByOffset.x > endOffsetX) {
                if (index > 0) {
                    nearByOffset = [self offsetWithCenterCoverViewIndex:(index-1)];
                }
            }
        }
        
        //
        *targetContentOffset = nearByOffset;
    }
}

#pragma mark Private methods
- (void)setup
{
    _scrollView = [[CardFlowScrollView alloc] initWithFrame:self.bounds];
    _scrollView.parentView = self;
    _scrollView.delegate = self;
    _scrollView.bounces = NO;
    _scrollView.decelerationRate = 0.98f;
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:_scrollView];
    
    _numberOfCards = 0;
    _cardSize = CGSizeMake(CardWidth, CardHeight);
    _cardSpace = 0.0;
    _cardAngle = 0.0;
    _cardScale = 1.1;
}

- (NSUInteger)nearByIndexOfScrollViewContentOffset:(CGPoint)contentOffset
{
    NSInteger index = nearbyintf(contentOffset.x / (self.cardSize.width + self.cardSpace));
    return MIN(MAX(0, index), self.numberOfCards-1);
}

- (CGPoint)nearByOffsetOfScrollViewContentOffset:(CGPoint)contentOffset
{
    NSInteger index = [self nearByIndexOfScrollViewContentOffset:contentOffset];
    return CGPointMake(index*(self.cardSize.width + self.cardSpace), contentOffset.y);
}

- (CGPoint)offsetWithCenterCoverViewIndex:(NSInteger)index
{
    return CGPointMake(index*(self.cardSize.width + self.cardSpace), _scrollView.contentOffset.y);
}

@end


#pragma mark - Card View
@implementation CardView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self addSubview:_imageView];
    
//    self.layer.masksToBounds = NO;
//    self.layer.cornerRadius = 2;
//    //默认是0,-3。下面设置的是左阴影
//    self.layer.shadowOffset = CGSizeMake(0.0, 0.0);
//    self.layer.shadowRadius = 3.0;
////    self.layer.shadowColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.5].CGColor;
//    self.layer.shadowColor = [UIColor redColor].CGColor;
//    self.layer.shadowPath =[UIBezierPath bezierPathWithRect:self.bounds].CGPath;

}

- (void)setBackgroundView:(UIView *)backgroundView
{
    _backgroundView = backgroundView;
    [self insertSubview:_backgroundView atIndex:0];
}

- (void)dealloc
{
    [_imageView PM_RELEASE];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}

@end








